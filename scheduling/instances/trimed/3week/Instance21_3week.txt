# This is a comment. Comments start with #
SECTION_HORIZON
# All instances start on a Monday
# The horizon length in days:
21

SECTION_SHIFTS
# ShiftID, Length in mins, Shifts which cannot follow this shift | separated
a1,480,
a2,720,a1|a2
p1,480,a1|a2|d1|d2|d3|p2
p2,720,a1|a2|d1|d2|d3|p2
n1,480,a1|a2|d1|d2|d3|p1|p2
d1,480,a1|a2
d2,480,a1|a2
d3,480,a1|a2

SECTION_STAFF
# ID, MaxShifts, MaxTotalMinutes, MinTotalMinutes, MaxConsecutiveShifts, MinConsecutiveShifts, MinConsecutiveDaysOff, MaxWeekends
AA,a1=21|a2=0|p1=21|p2=5|n1=0|d1=21|d2=21|d3=21,8100,4777,5,2,2,2
AB,a1=21|a2=5|p1=21|p2=5|n1=0|d1=21|d2=21|d3=21,8100,4777,5,2,2,2
AC,a1=21|a2=0|p1=21|p2=5|n1=5|d1=0|d2=21|d3=0,8100,4777,5,2,2,2
AD,a1=21|a2=0|p1=21|p2=5|n1=0|d1=21|d2=0|d3=21,8100,4777,5,2,2,2
AE,a1=21|a2=5|p1=21|p2=0|n1=0|d1=21|d2=21|d3=0,8100,4777,5,2,2,2
AF,a1=21|a2=5|p1=0|p2=5|n1=0|d1=0|d2=21|d3=0,8100,4777,5,2,2,2
AG,a1=21|a2=5|p1=0|p2=5|n1=5|d1=0|d2=21|d3=21,8100,4777,5,2,2,2
AH,a1=21|a2=5|p1=21|p2=0|n1=0|d1=21|d2=0|d3=21,8100,4777,5,2,2,2
AI,a1=21|a2=5|p1=21|p2=5|n1=5|d1=21|d2=21|d3=0,8100,4777,5,2,2,2
AJ,a1=21|a2=5|p1=21|p2=0|n1=5|d1=21|d2=21|d3=21,8100,4777,5,2,2,2
AK,a1=21|a2=5|p1=0|p2=0|n1=5|d1=21|d2=0|d3=21,8100,4777,5,2,2,2
AL,a1=21|a2=0|p1=21|p2=5|n1=5|d1=21|d2=0|d3=21,8100,4777,5,2,2,2
AM,a1=21|a2=5|p1=21|p2=0|n1=5|d1=21|d2=0|d3=21,8100,4777,5,2,2,2
AN,a1=21|a2=5|p1=0|p2=5|n1=5|d1=21|d2=21|d3=0,8100,4777,5,2,2,2
AO,a1=21|a2=5|p1=21|p2=5|n1=5|d1=21|d2=21|d3=21,8100,4777,6,2,2,2
AP,a1=21|a2=5|p1=21|p2=5|n1=5|d1=0|d2=0|d3=0,8100,4777,6,2,2,2
AQ,a1=21|a2=5|p1=21|p2=5|n1=5|d1=0|d2=21|d3=0,8100,4777,6,2,2,2
AR,a1=21|a2=5|p1=0|p2=5|n1=5|d1=21|d2=0|d3=0,8100,4777,6,2,2,2
AS,a1=0|a2=0|p1=21|p2=5|n1=5|d1=21|d2=21|d3=0,8100,4777,6,2,2,2
AT,a1=0|a2=0|p1=21|p2=0|n1=5|d1=0|d2=21|d3=21,8100,4777,6,2,2,2
AU,a1=0|a2=5|p1=21|p2=5|n1=0|d1=0|d2=21|d3=21,8100,4777,6,2,2,2
AV,a1=0|a2=5|p1=21|p2=5|n1=0|d1=0|d2=0|d3=21,8100,4777,6,2,2,2
AW,a1=21|a2=0|p1=21|p2=0|n1=5|d1=21|d2=0|d3=21,8100,4777,6,2,2,2
AX,a1=21|a2=5|p1=21|p2=5|n1=0|d1=21|d2=0|d3=21,8100,4777,6,2,2,2
AY,a1=21|a2=5|p1=21|p2=5|n1=5|d1=21|d2=0|d3=0,8100,4777,6,2,2,2
AZ,a1=0|a2=5|p1=21|p2=5|n1=5|d1=0|d2=21|d3=21,8100,4777,6,2,2,2
BA,a1=21|a2=5|p1=21|p2=0|n1=5|d1=21|d2=21|d3=21,8100,4777,6,2,2,2
BB,a1=21|a2=5|p1=0|p2=5|n1=0|d1=21|d2=21|d3=0,8100,4777,6,2,2,2
A,a1=21|a2=5|p1=21|p2=5|n1=0|d1=0|d2=0|d3=0,8100,4777,5,2,2,2
BC,a1=21|a2=5|p1=0|p2=5|n1=5|d1=21|d2=0|d3=21,8100,4777,6,2,2,2
B,a1=21|a2=5|p1=21|p2=5|n1=5|d1=0|d2=21|d3=21,8100,4777,5,2,2,2
BD,a1=21|a2=5|p1=0|p2=5|n1=5|d1=21|d2=21|d3=21,8100,4777,6,2,2,2
C,a1=21|a2=0|p1=0|p2=5|n1=5|d1=21|d2=0|d3=21,8100,4777,5,2,2,2
BE,a1=0|a2=5|p1=21|p2=5|n1=0|d1=21|d2=21|d3=21,8100,4777,6,2,2,2
D,a1=0|a2=0|p1=21|p2=5|n1=0|d1=21|d2=21|d3=21,8100,4777,5,2,2,2
BF,a1=0|a2=5|p1=21|p2=5|n1=0|d1=21|d2=0|d3=21,8100,4777,6,2,2,2
E,a1=0|a2=5|p1=21|p2=5|n1=0|d1=21|d2=21|d3=21,8100,4777,5,2,2,2
BG,a1=21|a2=5|p1=0|p2=5|n1=5|d1=21|d2=0|d3=0,8100,4777,6,2,2,2
F,a1=21|a2=5|p1=21|p2=0|n1=5|d1=21|d2=21|d3=21,8100,4777,5,2,2,2
BH,a1=21|a2=5|p1=0|p2=5|n1=5|d1=21|d2=21|d3=21,8100,4777,6,2,2,2
G,a1=21|a2=0|p1=21|p2=0|n1=5|d1=0|d2=21|d3=0,8100,4777,5,2,2,2
BI,a1=0|a2=0|p1=21|p2=0|n1=0|d1=21|d2=21|d3=21,8100,4777,6,2,2,2
H,a1=21|a2=5|p1=21|p2=0|n1=5|d1=21|d2=21|d3=21,8100,4777,5,2,2,2
BJ,a1=0|a2=0|p1=21|p2=5|n1=5|d1=21|d2=21|d3=21,8100,4777,6,2,2,2
I,a1=21|a2=5|p1=21|p2=0|n1=5|d1=21|d2=21|d3=21,8100,4777,5,2,2,2
BK,a1=21|a2=0|p1=0|p2=5|n1=0|d1=21|d2=0|d3=21,8100,4777,6,2,2,2
J,a1=0|a2=5|p1=21|p2=5|n1=0|d1=0|d2=21|d3=21,8100,4777,5,2,2,2
BL,a1=21|a2=0|p1=21|p2=5|n1=5|d1=0|d2=21|d3=0,8100,4777,6,2,2,2
K,a1=21|a2=0|p1=0|p2=5|n1=5|d1=0|d2=21|d3=21,8100,4777,5,2,2,2
BM,a1=21|a2=0|p1=0|p2=5|n1=0|d1=21|d2=21|d3=21,8100,4777,6,2,2,2
L,a1=21|a2=0|p1=21|p2=0|n1=5|d1=21|d2=0|d3=21,8100,4777,5,2,2,2
BN,a1=21|a2=5|p1=0|p2=0|n1=5|d1=21|d2=0|d3=21,8100,4777,6,2,2,2
M,a1=21|a2=5|p1=0|p2=5|n1=0|d1=21|d2=21|d3=0,8100,4777,5,2,2,2
BO,a1=21|a2=5|p1=21|p2=0|n1=5|d1=21|d2=0|d3=21,8100,4777,6,2,2,2
N,a1=21|a2=5|p1=21|p2=0|n1=5|d1=21|d2=21|d3=21,8100,4777,5,2,2,2
BP,a1=21|a2=5|p1=21|p2=5|n1=5|d1=21|d2=21|d3=0,8100,4777,6,2,2,2
O,a1=21|a2=5|p1=21|p2=0|n1=0|d1=21|d2=0|d3=21,8100,4777,5,2,2,2
BQ,a1=21|a2=0|p1=21|p2=0|n1=5|d1=21|d2=21|d3=21,8100,4777,6,2,2,2
P,a1=21|a2=0|p1=0|p2=5|n1=5|d1=0|d2=21|d3=0,8100,4777,5,2,2,2
BR,a1=21|a2=0|p1=21|p2=5|n1=0|d1=21|d2=21|d3=21,8100,4777,6,2,2,2
Q,a1=0|a2=5|p1=21|p2=0|n1=5|d1=0|d2=0|d3=21,8100,4777,5,2,2,2
BS,a1=0|a2=5|p1=21|p2=5|n1=5|d1=21|d2=21|d3=21,7589,4471,5,3,2,2
R,a1=21|a2=0|p1=21|p2=5|n1=5|d1=21|d2=21|d3=0,8100,4777,5,2,2,2
BT,a1=0|a2=5|p1=21|p2=5|n1=0|d1=21|d2=21|d3=21,7589,4471,5,3,2,2
S,a1=21|a2=5|p1=21|p2=0|n1=5|d1=21|d2=21|d3=0,8100,4777,5,2,2,2
BU,a1=21|a2=0|p1=0|p2=5|n1=5|d1=21|d2=21|d3=21,7589,4471,5,3,2,2
T,a1=21|a2=5|p1=21|p2=5|n1=5|d1=0|d2=21|d3=21,8100,4777,5,2,2,2
BV,a1=21|a2=5|p1=21|p2=0|n1=5|d1=21|d2=21|d3=21,7589,4471,5,3,2,2
U,a1=0|a2=5|p1=21|p2=0|n1=0|d1=21|d2=21|d3=21,8100,4777,5,2,2,2
BW,a1=0|a2=5|p1=21|p2=5|n1=0|d1=0|d2=21|d3=21,7589,4471,5,3,2,2
V,a1=0|a2=5|p1=21|p2=5|n1=5|d1=0|d2=0|d3=21,8100,4777,5,2,2,2
BX,a1=21|a2=0|p1=0|p2=0|n1=0|d1=0|d2=21|d3=21,7589,4471,5,3,2,2
W,a1=21|a2=0|p1=21|p2=0|n1=0|d1=0|d2=0|d3=21,8100,4777,5,2,2,2
BY,a1=0|a2=5|p1=0|p2=5|n1=5|d1=21|d2=21|d3=0,7589,4471,5,3,2,2
X,a1=0|a2=5|p1=21|p2=5|n1=0|d1=21|d2=21|d3=21,8100,4777,5,2,2,2
BZ,a1=21|a2=5|p1=0|p2=5|n1=5|d1=0|d2=21|d3=21,7589,4471,5,3,2,2
Y,a1=21|a2=0|p1=21|p2=5|n1=0|d1=0|d2=21|d3=0,8100,4777,5,2,2,2
Z,a1=21|a2=0|p1=0|p2=5|n1=0|d1=0|d2=0|d3=21,8100,4777,5,2,2,2
CA,a1=0|a2=5|p1=21|p2=5|n1=5|d1=21|d2=21|d3=21,7589,4471,5,3,2,2
CB,a1=21|a2=0|p1=21|p2=5|n1=0|d1=21|d2=21|d3=21,7589,4471,5,3,2,2
CC,a1=21|a2=0|p1=21|p2=0|n1=0|d1=21|d2=21|d3=21,4448,2586,5,2,2,1
CD,a1=21|a2=3|p1=0|p2=3|n1=3|d1=21|d2=21|d3=21,4448,2586,5,2,2,1
CE,a1=0|a2=3|p1=21|p2=3|n1=3|d1=0|d2=21|d3=21,4448,2586,5,2,2,1
CF,a1=21|a2=3|p1=21|p2=0|n1=3|d1=21|d2=0|d3=21,4448,2586,5,2,2,1
CG,a1=21|a2=0|p1=21|p2=3|n1=3|d1=21|d2=0|d3=21,4448,2586,5,2,2,1
CH,a1=21|a2=3|p1=21|p2=0|n1=3|d1=0|d2=21|d3=21,4448,2586,5,2,2,1
CI,a1=21|a2=3|p1=0|p2=3|n1=3|d1=21|d2=21|d3=21,4448,2586,5,2,2,1
CJ,a1=21|a2=3|p1=21|p2=0|n1=3|d1=21|d2=21|d3=21,4448,2586,5,2,2,1
CK,a1=21|a2=0|p1=21|p2=0|n1=3|d1=21|d2=21|d3=0,4448,2586,5,2,2,1
CL,a1=21|a2=3|p1=21|p2=0|n1=3|d1=0|d2=0|d3=0,4448,2586,5,2,2,1
CM,a1=21|a2=3|p1=21|p2=3|n1=3|d1=21|d2=0|d3=21,4050,2347,3,1,2,1
CN,a1=21|a2=3|p1=21|p2=0|n1=3|d1=21|d2=21|d3=0,4050,2347,3,1,2,1
CO,a1=21|a2=3|p1=21|p2=0|n1=0|d1=0|d2=21|d3=21,4050,2347,3,1,2,1
CP,a1=21|a2=3|p1=21|p2=0|n1=0|d1=21|d2=21|d3=21,4050,2347,3,1,2,1
CQ,a1=21|a2=0|p1=21|p2=3|n1=3|d1=0|d2=21|d3=0,4050,2347,3,1,2,1
CR,a1=0|a2=3|p1=0|p2=0|n1=3|d1=0|d2=21|d3=0,4050,2347,3,1,2,1
CS,a1=21|a2=3|p1=21|p2=3|n1=0|d1=21|d2=21|d3=0,4050,2347,3,1,2,1
CT,a1=0|a2=0|p1=0|p2=0|n1=0|d1=21|d2=21|d3=21,4050,2347,3,1,2,1
CU,a1=21|a2=3|p1=21|p2=0|n1=3|d1=21|d2=0|d3=0,4050,2347,3,1,2,1
CV,a1=21|a2=3|p1=21|p2=0|n1=3|d1=21|d2=0|d3=21,4050,2347,3,1,2,1

SECTION_DAYS_OFF
# EmployeeID, DayIndexes (start at zero)
AA
AB
AC
AD
AE
AF
AG
AH
AI
AJ
AK
AL
AM
AN
AO
AP
AQ
AR
AS
AT
AU
AV
AW
AX
AY
AZ
BA
BB
A
BC
B
BD
C
BE
D
BF
E
BG
F
BH
G
BI
H
BJ
I
BK
J
BL
K
BM
L
BN
M
BO
N
BP
O
BQ
P
BR
Q
BS
R
BT
S
BU
T
BV
U
BW
V
BX
W
BY
X
BZ
Y
Z
CA
CB
CC
CD
CE
CF
CG
CH
CI
CJ
CK
CL
CM
CN
CO
CP
CQ
CR
CS
CT
CU
CV

SECTION_SHIFT_ON_REQUESTS
# EmployeeID, Day, ShiftID, Weight
A,0,p2,3
A,1,p2,3
A,5,a2,3
A,6,a2,3
A,7,a2,3
A,8,a2,3
A,9,a2,3
A,17,a2,3
A,18,a2,3
B,3,a2,3
B,4,a2,3
B,5,a2,3
B,9,p2,1
B,10,p2,1
B,11,p2,1
B,12,p2,1
B,20,n1,1
C,8,n1,2
C,9,n1,2
C,10,n1,2
C,11,n1,2
E,19,d2,3
E,20,d2,3
F,11,a2,3
F,18,a2,1
F,19,a2,1
F,20,a2,1
G,7,d2,2
G,8,d2,2
H,8,p1,3
H,9,p1,3
H,10,p1,3
H,11,p1,3
H,12,p1,3
H,17,p1,1
J,7,d2,1
J,8,d2,1
J,9,d2,1
J,16,a2,1
J,17,a2,1
J,18,a2,1
J,19,a2,1
J,20,a2,1
K,12,p2,3
K,13,p2,3
L,1,a1,1
M,2,d1,3
M,3,d1,3
M,9,a1,1
M,10,a1,1
M,11,a1,1
M,12,a1,1
M,13,a1,1
N,7,d2,1
N,8,d2,1
N,9,d2,1
N,10,d2,1
N,11,d2,1
N,19,p1,2
N,20,p1,2
O,7,d1,2
O,8,d1,2
O,9,d1,2
O,10,d1,2
O,11,d1,2
P,0,n1,2
P,8,p2,2
P,9,p2,2
P,16,n1,3
P,17,n1,3
P,18,n1,3
P,19,n1,3
Q,3,p1,1
Q,4,p1,1
Q,5,p1,1
Q,6,p1,1
R,3,n1,2
R,11,n1,2
R,12,n1,2
R,13,n1,2
S,9,a2,2
S,10,a2,2
S,11,a2,2
U,10,d2,2
U,11,d2,2
U,12,d2,2
U,13,d2,2
U,14,d2,2
V,18,a2,1
V,19,a2,1
X,3,p2,1
X,4,p2,1
X,5,p2,1
X,6,p2,1
X,7,p2,1
X,12,d1,3
X,13,d1,3
X,14,d1,3
Y,3,a1,1
Y,4,a1,1
Y,5,a1,1
Y,6,a1,1
Y,19,a1,2
Z,11,d3,2
Z,12,d3,2
Z,13,d3,2
AB,0,p2,3
AB,1,p2,3
AB,2,p2,3
AC,9,n1,1
AE,2,a1,3
AE,10,a2,1
AE,11,a2,1
AE,12,a2,1
AE,20,d2,3
AF,0,d2,1
AF,1,d2,1
AF,20,d2,2
AH,10,a1,1
AH,11,a1,1
AH,12,a1,1
AH,13,a1,1
AJ,6,d3,1
AJ,7,d3,1
AJ,8,d3,1
AJ,9,d3,1
AJ,17,d3,3
AJ,18,d3,3
AJ,19,d3,3
AJ,20,d3,3
AK,9,n1,2
AK,10,n1,2
AK,11,n1,2
AL,19,p1,3
AL,20,p1,3
AM,13,d1,1
AM,14,d1,1
AM,15,d1,1
AM,16,d1,1
AM,17,d1,1
AN,0,d2,1
AN,1,d2,1
AN,2,d2,1
AN,3,d2,1
AN,4,d2,1
AN,9,a1,2
AN,10,a1,2
AN,11,a1,2
AN,12,a1,2
AN,13,a1,2
AP,9,a1,3
AP,10,a1,3
AP,11,a1,3
AP,12,a1,3
AP,17,p2,2
AP,18,p2,2
AP,19,p2,2
AP,20,p2,2
AQ,2,p2,3
AQ,3,p2,3
AQ,4,p2,3
AQ,12,p2,1
AQ,13,p2,1
AQ,14,p2,1
AQ,15,p2,1
AQ,16,p2,1
AR,10,a1,1
AR,11,a1,1
AR,12,a1,1
AR,13,a1,1
AR,14,a1,1
AR,20,a1,1
AS,3,n1,2
AS,4,n1,2
AS,17,n1,1
AS,18,n1,1
AS,19,n1,1
AS,20,n1,1
AT,2,d3,1
AT,3,d3,1
AT,4,d3,1
AT,5,d3,1
AT,6,d3,1
AT,17,d2,1
AT,18,d2,1
AT,19,d2,1
AU,9,d2,3
AU,10,d2,3
AU,11,d2,3
AU,20,d3,2
AV,3,p2,3
AV,4,p2,3
AV,5,p2,3
AV,6,p2,3
AV,7,p2,3
AV,17,p1,2
AV,18,p1,2
AW,0,p1,2
AW,11,d3,3
AW,12,d3,3
AX,3,a1,1
AX,9,p1,1
AY,3,p1,3
AY,4,p1,3
AY,5,p1,3
AY,13,p2,1
AY,14,p2,1
AY,15,p2,1
AZ,1,p1,1
AZ,2,p1,1
AZ,3,p1,1
AZ,4,p1,1
AZ,5,p1,1
AZ,17,p1,1
AZ,18,p1,1
AZ,19,p1,1
AZ,20,p1,1
BA,1,a1,2
BA,2,a1,2
BA,3,a1,2
BA,15,d1,2
BA,16,d1,2
BA,17,d1,2
BA,18,d1,2
BA,19,d1,2
BB,18,a2,2
BB,19,a2,2
BB,20,a2,2
BC,0,a1,2
BC,8,n1,3
BC,9,n1,3
BC,10,n1,3
BC,11,n1,3
BD,6,a1,3
BD,7,a1,3
BD,8,a1,3
BD,20,d1,3
BE,0,d3,3
BE,1,d3,3
BE,2,d3,3
BE,3,d3,3
BE,4,d3,3
BE,9,d1,2
BE,10,d1,2
BE,11,d1,2
BE,12,d1,2
BE,20,d3,1
BF,3,p2,1
BG,15,a1,1
BG,16,a1,1
BG,17,a1,1
BG,18,a1,1
BG,19,a1,1
BH,14,d3,1
BI,1,d1,3
BI,2,d1,3
BI,3,d1,3
BI,13,d1,2
BI,14,d1,2
BK,4,p2,2
BK,5,p2,2
BL,3,d2,2
BL,4,d2,2
BL,8,n1,2
BL,13,d2,3
BM,13,a1,3
BM,14,a1,3
BM,15,a1,3
BM,19,d2,3
BM,20,d2,3
BN,5,n1,1
BN,6,n1,1
BN,17,d1,2
BN,18,d1,2
BN,19,d1,2
BN,20,d1,2
BO,1,d3,1
BO,2,d3,1
BO,3,d3,1
BO,4,d3,1
BP,12,p1,2
BP,13,p1,2
BP,14,p1,2
BP,15,p1,2
BP,16,p1,2
BQ,9,a1,1
BQ,10,a1,1
BQ,11,a1,1
BQ,12,a1,1
BQ,13,a1,1
BQ,20,d2,2
BR,12,p2,1
BR,13,p2,1
BR,14,p2,1
BR,20,d2,3
BS,7,p1,2
BT,0,a2,2
BT,20,p2,1
BU,7,a1,3
BU,8,a1,3
BU,9,a1,3
BU,16,d2,3
BU,17,d2,3
BU,18,d2,3
BU,19,d2,3
BV,19,p1,3
BV,20,p1,3
BW,0,a2,1
BW,4,a2,1
BW,5,a2,1
BW,13,p1,3
BW,18,p1,3
BW,19,p1,3
BX,19,d3,2
BX,20,d3,2
BY,2,d1,3
BY,3,d1,3
BY,4,d1,3
BY,5,d1,3
BY,6,d1,3
BZ,7,a2,2
BZ,8,a2,2
CA,8,p2,2
CA,9,p2,2
CC,10,d2,3
CD,10,a1,3
CD,11,a1,3
CD,12,a1,3
CE,14,n1,1
CE,15,n1,1
CE,16,n1,1
CE,17,n1,1
CE,18,n1,1
CF,0,n1,1
CF,1,n1,1
CF,5,n1,1
CF,6,n1,1
CF,7,n1,1
CF,8,n1,1
CF,9,n1,1
CG,5,a1,3
CG,6,a1,3
CG,7,a1,3
CH,8,p1,2
CH,9,p1,2
CH,15,a1,2
CH,16,a1,2
CH,17,a1,2
CI,3,d2,2
CI,4,d2,2
CI,5,d2,2
CI,6,d2,2
CI,7,d2,2
CI,19,n1,3
CJ,1,d3,2
CJ,2,d3,2
CJ,16,a2,3
CJ,17,a2,3
CJ,18,a2,3
CJ,19,a2,3
CK,2,n1,2
CK,3,n1,2
CK,4,n1,2
CL,19,a1,3
CM,3,p2,2
CM,4,p2,2
CM,5,p2,2
CO,3,p1,3
CO,4,p1,3
CO,8,a2,2
CO,9,a2,2
CO,10,a2,2
CP,0,p1,1
CP,1,p1,1
CP,2,p1,1
CP,3,p1,1
CP,10,d3,1
CP,11,d3,1
CQ,0,p2,1
CQ,1,p2,1
CS,4,a1,2
CT,0,d2,2
CT,1,d2,2
CT,2,d2,2
CT,3,d2,2
CT,4,d2,2
CU,16,a1,1
CU,17,a1,1
CV,13,d1,2
CV,14,d1,2
CV,15,d1,2
CV,16,d1,2

SECTION_SHIFT_OFF_REQUESTS
# EmployeeID, Day, ShiftID, Weight
C,17,p2,3
C,18,p2,3
C,19,p2,3
C,20,p2,3
D,0,d3,3
D,1,d3,3
D,2,d3,3
D,10,d3,2
E,8,p1,1
E,9,p1,1
E,10,p1,1
E,11,p1,1
E,12,p1,1
G,16,p1,1
I,3,a2,3
I,4,a2,3
I,5,a2,3
I,6,a2,3
L,9,a1,2
L,10,a1,2
L,11,a1,2
L,12,a1,2
L,13,a1,2
Q,19,p1,2
Q,20,p1,2
S,17,a1,3
S,18,a1,3
S,19,a1,3
S,20,a1,3
T,18,p2,3
T,19,p2,3
T,20,p2,3
U,1,d3,3
U,2,d3,3
U,3,d3,3
U,4,d3,3
V,2,n1,2
V,3,n1,2
V,4,n1,2
W,19,p1,2
Z,3,p2,1
Z,4,p2,1
AA,5,d2,3
AA,6,d2,3
AA,7,d2,3
AA,8,d2,3
AA,9,d2,3
AB,12,a1,3
AB,13,a1,3
AB,14,a1,3
AD,0,d1,3
AD,1,d1,3
AD,2,d1,3
AD,3,d1,3
AF,5,a2,2
AG,7,a2,2
AG,8,a2,2
AG,9,a2,2
AG,10,a2,2
AG,11,a2,2
AI,3,p1,3
AI,4,p1,3
AI,16,p2,2
AI,20,d2,2
AK,4,a2,2
AK,17,d1,2
AK,18,d1,2
AM,5,d3,2
AM,6,d3,2
AM,7,d3,2
AM,8,d3,2
AM,9,d3,2
AS,9,d2,3
AS,10,d2,3
AW,17,d3,3
AW,18,d3,3
AW,19,d3,3
AW,20,d3,3
AZ,12,a2,2
AZ,13,a2,2
BA,7,n1,2
BC,15,a2,1
BC,16,a2,1
BF,14,d3,1
BF,15,d3,1
BF,16,d3,1
BF,17,d3,1
BH,6,a2,3
BH,7,a2,3
BH,8,a2,3
BH,9,a2,3
BH,10,a2,3
BJ,5,n1,2
BJ,6,n1,2
BL,18,p1,2
BL,19,p1,2
BM,6,p2,3
BM,7,p2,3
BM,8,p2,3
BM,9,p2,3
BO,14,p1,2
BO,15,p1,2
BO,16,p1,2
BO,17,p1,2
BO,18,p1,2
BP,1,p2,2
BP,2,p2,2
BP,3,p2,2
BP,4,p2,2
BQ,1,a1,2
BR,2,d1,1
BR,3,d1,1
BR,4,d1,1
BR,5,d1,1
BR,6,d1,1
BS,1,d1,3
BS,2,d1,3
BV,3,p1,3
BV,4,p1,3
BV,5,p1,3
BV,6,p1,3
BV,7,p1,3
BX,3,d2,3
BX,4,d2,3
BX,5,d2,3
BX,6,d2,3
BX,7,d2,3
BY,19,d2,3
BY,20,d2,3
BZ,15,a2,3
BZ,16,a2,3
BZ,17,a2,3
CC,0,d1,2
CC,1,d1,2
CC,2,d1,2
CC,3,d1,2
CC,4,d1,2
CD,6,d1,2
CG,12,p2,3
CG,13,p2,3
CG,14,p2,3
CG,15,p2,3
CK,9,d1,1
CK,10,d1,1
CK,11,d1,1
CK,12,d1,1
CK,13,d1,1
CM,12,d1,3
CM,13,d1,3
CM,14,d1,3
CM,15,d1,3
CM,16,d1,3
CN,2,a1,1
CN,3,a1,1
CN,4,a1,1
CN,5,a1,1
CN,6,a1,1
CN,11,a1,1
CN,12,a1,1
CN,13,a1,1
CN,14,a1,1
CN,20,a1,1
CP,17,a2,2
CQ,7,n1,2
CQ,8,n1,2
CQ,9,n1,2
CQ,10,n1,2
CQ,11,n1,2
CQ,19,d2,3
CR,0,n1,3
CR,1,n1,3
CR,2,n1,3
CR,3,n1,3
CR,4,n1,3
CS,16,d2,2
CS,17,d2,2
CS,18,d2,2
CS,19,d2,2
CS,20,d2,2
CT,16,d1,2
CT,17,d1,2
CT,18,d1,2
CT,19,d1,2
CU,10,p1,2
CV,1,p1,1
CV,2,p1,1
CV,3,p1,1
CV,4,p1,1
AA,19,a1,50
AA,19,a2,50
AA,19,p1,50
AA,19,p2,50
AA,19,n1,50
AA,19,d1,50
AA,19,d2,50
AA,19,d3,50
AA,20,a1,50
AA,20,a2,50
AA,20,p1,50
AA,20,p2,50
AA,20,n1,50
AA,20,d1,50
AA,20,d2,50
AA,20,d3,50
AG,13,a1,50
AG,13,a2,50
AG,13,p1,50
AG,13,p2,50
AG,13,n1,50
AG,13,d1,50
AG,13,d2,50
AG,13,d3,50
AG,14,a1,50
AG,14,a2,50
AG,14,p1,50
AG,14,p2,50
AG,14,n1,50
AG,14,d1,50
AG,14,d2,50
AG,14,d3,50
AG,15,a1,50
AG,15,a2,50
AG,15,p1,50
AG,15,p2,50
AG,15,n1,50
AG,15,d1,50
AG,15,d2,50
AG,15,d3,50
AG,16,a1,50
AG,16,a2,50
AG,16,p1,50
AG,16,p2,50
AG,16,n1,50
AG,16,d1,50
AG,16,d2,50
AG,16,d3,50
AG,17,a1,50
AG,17,a2,50
AG,17,p1,50
AG,17,p2,50
AG,17,n1,50
AG,17,d1,50
AG,17,d2,50
AG,17,d3,50
AG,18,a1,50
AG,18,a2,50
AG,18,p1,50
AG,18,p2,50
AG,18,n1,50
AG,18,d1,50
AG,18,d2,50
AG,18,d3,50
AI,10,a1,50
AI,10,a2,50
AI,10,p1,50
AI,10,p2,50
AI,10,n1,50
AI,10,d1,50
AI,10,d2,50
AI,10,d3,50
AI,11,a1,50
AI,11,a2,50
AI,11,p1,50
AI,11,p2,50
AI,11,n1,50
AI,11,d1,50
AI,11,d2,50
AI,11,d3,50
AJ,10,a1,50
AJ,10,a2,50
AJ,10,p1,50
AJ,10,p2,50
AJ,10,n1,50
AJ,10,d1,50
AJ,10,d2,50
AJ,10,d3,50
AJ,15,a1,50
AJ,15,a2,50
AJ,15,p1,50
AJ,15,p2,50
AJ,15,n1,50
AJ,15,d1,50
AJ,15,d2,50
AJ,15,d3,50
AL,3,a1,50
AL,3,a2,50
AL,3,p1,50
AL,3,p2,50
AL,3,n1,50
AL,3,d1,50
AL,3,d2,50
AL,3,d3,50
AL,4,a1,50
AL,4,a2,50
AL,4,p1,50
AL,4,p2,50
AL,4,n1,50
AL,4,d1,50
AL,4,d2,50
AL,4,d3,50
AL,5,a1,50
AL,5,a2,50
AL,5,p1,50
AL,5,p2,50
AL,5,n1,50
AL,5,d1,50
AL,5,d2,50
AL,5,d3,50
AL,6,a1,50
AL,6,a2,50
AL,6,p1,50
AL,6,p2,50
AL,6,n1,50
AL,6,d1,50
AL,6,d2,50
AL,6,d3,50
AL,7,a1,50
AL,7,a2,50
AL,7,p1,50
AL,7,p2,50
AL,7,n1,50
AL,7,d1,50
AL,7,d2,50
AL,7,d3,50
AL,8,a1,50
AL,8,a2,50
AL,8,p1,50
AL,8,p2,50
AL,8,n1,50
AL,8,d1,50
AL,8,d2,50
AL,8,d3,50
AL,9,a1,50
AL,9,a2,50
AL,9,p1,50
AL,9,p2,50
AL,9,n1,50
AL,9,d1,50
AL,9,d2,50
AL,9,d3,50
AL,10,a1,50
AL,10,a2,50
AL,10,p1,50
AL,10,p2,50
AL,10,n1,50
AL,10,d1,50
AL,10,d2,50
AL,10,d3,50
AL,11,a1,50
AL,11,a2,50
AL,11,p1,50
AL,11,p2,50
AL,11,n1,50
AL,11,d1,50
AL,11,d2,50
AL,11,d3,50
AL,12,a1,50
AL,12,a2,50
AL,12,p1,50
AL,12,p2,50
AL,12,n1,50
AL,12,d1,50
AL,12,d2,50
AL,12,d3,50
AL,13,a1,50
AL,13,a2,50
AL,13,p1,50
AL,13,p2,50
AL,13,n1,50
AL,13,d1,50
AL,13,d2,50
AL,13,d3,50
AM,18,a1,50
AM,18,a2,50
AM,18,p1,50
AM,18,p2,50
AM,18,n1,50
AM,18,d1,50
AM,18,d2,50
AM,18,d3,50
AM,19,a1,50
AM,19,a2,50
AM,19,p1,50
AM,19,p2,50
AM,19,n1,50
AM,19,d1,50
AM,19,d2,50
AM,19,d3,50
AM,20,a1,50
AM,20,a2,50
AM,20,p1,50
AM,20,p2,50
AM,20,n1,50
AM,20,d1,50
AM,20,d2,50
AM,20,d3,50
AP,2,a1,50
AP,2,a2,50
AP,2,p1,50
AP,2,p2,50
AP,2,n1,50
AP,2,d1,50
AP,2,d2,50
AP,2,d3,50
AP,3,a1,50
AP,3,a2,50
AP,3,p1,50
AP,3,p2,50
AP,3,n1,50
AP,3,d1,50
AP,3,d2,50
AP,3,d3,50
AP,6,a1,50
AP,6,a2,50
AP,6,p1,50
AP,6,p2,50
AP,6,n1,50
AP,6,d1,50
AP,6,d2,50
AP,6,d3,50
AZ,8,a1,50
AZ,8,a2,50
AZ,8,p1,50
AZ,8,p2,50
AZ,8,n1,50
AZ,8,d1,50
AZ,8,d2,50
AZ,8,d3,50
AZ,9,a1,50
AZ,9,a2,50
AZ,9,p1,50
AZ,9,p2,50
AZ,9,n1,50
AZ,9,d1,50
AZ,9,d2,50
AZ,9,d3,50
AZ,10,a1,50
AZ,10,a2,50
AZ,10,p1,50
AZ,10,p2,50
AZ,10,n1,50
AZ,10,d1,50
AZ,10,d2,50
AZ,10,d3,50
AZ,11,a1,50
AZ,11,a2,50
AZ,11,p1,50
AZ,11,p2,50
AZ,11,n1,50
AZ,11,d1,50
AZ,11,d2,50
AZ,11,d3,50
BA,4,a1,50
BA,4,a2,50
BA,4,p1,50
BA,4,p2,50
BA,4,n1,50
BA,4,d1,50
BA,4,d2,50
BA,4,d3,50
BA,20,a1,50
BA,20,a2,50
BA,20,p1,50
BA,20,p2,50
BA,20,n1,50
BA,20,d1,50
BA,20,d2,50
BA,20,d3,50
BB,11,a1,50
BB,11,a2,50
BB,11,p1,50
BB,11,p2,50
BB,11,n1,50
BB,11,d1,50
BB,11,d2,50
BB,11,d3,50
BB,12,a1,50
BB,12,a2,50
BB,12,p1,50
BB,12,p2,50
BB,12,n1,50
BB,12,d1,50
BB,12,d2,50
BB,12,d3,50
BB,13,a1,50
BB,13,a2,50
BB,13,p1,50
BB,13,p2,50
BB,13,n1,50
BB,13,d1,50
BB,13,d2,50
BB,13,d3,50
BB,14,a1,50
BB,14,a2,50
BB,14,p1,50
BB,14,p2,50
BB,14,n1,50
BB,14,d1,50
BB,14,d2,50
BB,14,d3,50
BB,15,a1,50
BB,15,a2,50
BB,15,p1,50
BB,15,p2,50
BB,15,n1,50
BB,15,d1,50
BB,15,d2,50
BB,15,d3,50
BB,16,a1,50
BB,16,a2,50
BB,16,p1,50
BB,16,p2,50
BB,16,n1,50
BB,16,d1,50
BB,16,d2,50
BB,16,d3,50
BC,13,a1,50
BC,13,a2,50
BC,13,p1,50
BC,13,p2,50
BC,13,n1,50
BC,13,d1,50
BC,13,d2,50
BC,13,d3,50
BC,14,a1,50
BC,14,a2,50
BC,14,p1,50
BC,14,p2,50
BC,14,n1,50
BC,14,d1,50
BC,14,d2,50
BC,14,d3,50
BC,18,a1,50
BC,18,a2,50
BC,18,p1,50
BC,18,p2,50
BC,18,n1,50
BC,18,d1,50
BC,18,d2,50
BC,18,d3,50
B,0,a1,50
B,0,a2,50
B,0,p1,50
B,0,p2,50
B,0,n1,50
B,0,d1,50
B,0,d2,50
B,0,d3,50
B,1,a1,50
B,1,a2,50
B,1,p1,50
B,1,p2,50
B,1,n1,50
B,1,d1,50
B,1,d2,50
B,1,d3,50
B,2,a1,50
B,2,a2,50
B,2,p1,50
B,2,p2,50
B,2,n1,50
B,2,d1,50
B,2,d2,50
B,2,d3,50
B,16,a1,50
B,16,a2,50
B,16,p1,50
B,16,p2,50
B,16,n1,50
B,16,d1,50
B,16,d2,50
B,16,d3,50
B,17,a1,50
B,17,a2,50
B,17,p1,50
B,17,p2,50
B,17,n1,50
B,17,d1,50
B,17,d2,50
B,17,d3,50
B,18,a1,50
B,18,a2,50
B,18,p1,50
B,18,p2,50
B,18,n1,50
B,18,d1,50
B,18,d2,50
B,18,d3,50
B,19,a1,50
B,19,a2,50
B,19,p1,50
B,19,p2,50
B,19,n1,50
B,19,d1,50
B,19,d2,50
B,19,d3,50
C,3,a1,50
C,3,a2,50
C,3,p1,50
C,3,p2,50
C,3,n1,50
C,3,d1,50
C,3,d2,50
C,3,d3,50
C,4,a1,50
C,4,a2,50
C,4,p1,50
C,4,p2,50
C,4,n1,50
C,4,d1,50
C,4,d2,50
C,4,d3,50
C,5,a1,50
C,5,a2,50
C,5,p1,50
C,5,p2,50
C,5,n1,50
C,5,d1,50
C,5,d2,50
C,5,d3,50
C,6,a1,50
C,6,a2,50
C,6,p1,50
C,6,p2,50
C,6,n1,50
C,6,d1,50
C,6,d2,50
C,6,d3,50
D,5,a1,50
D,5,a2,50
D,5,p1,50
D,5,p2,50
D,5,n1,50
D,5,d1,50
D,5,d2,50
D,5,d3,50
BF,4,a1,50
BF,4,a2,50
BF,4,p1,50
BF,4,p2,50
BF,4,n1,50
BF,4,d1,50
BF,4,d2,50
BF,4,d3,50
BF,5,a1,50
BF,5,a2,50
BF,5,p1,50
BF,5,p2,50
BF,5,n1,50
BF,5,d1,50
BF,5,d2,50
BF,5,d3,50
BF,6,a1,50
BF,6,a2,50
BF,6,p1,50
BF,6,p2,50
BF,6,n1,50
BF,6,d1,50
BF,6,d2,50
BF,6,d3,50
BF,7,a1,50
BF,7,a2,50
BF,7,p1,50
BF,7,p2,50
BF,7,n1,50
BF,7,d1,50
BF,7,d2,50
BF,7,d3,50
BF,8,a1,50
BF,8,a2,50
BF,8,p1,50
BF,8,p2,50
BF,8,n1,50
BF,8,d1,50
BF,8,d2,50
BF,8,d3,50
BF,9,a1,50
BF,9,a2,50
BF,9,p1,50
BF,9,p2,50
BF,9,n1,50
BF,9,d1,50
BF,9,d2,50
BF,9,d3,50
F,3,a1,50
F,3,a2,50
F,3,p1,50
F,3,p2,50
F,3,n1,50
F,3,d1,50
F,3,d2,50
F,3,d3,50
F,4,a1,50
F,4,a2,50
F,4,p1,50
F,4,p2,50
F,4,n1,50
F,4,d1,50
F,4,d2,50
F,4,d3,50
F,5,a1,50
F,5,a2,50
F,5,p1,50
F,5,p2,50
F,5,n1,50
F,5,d1,50
F,5,d2,50
F,5,d3,50
F,6,a1,50
F,6,a2,50
F,6,p1,50
F,6,p2,50
F,6,n1,50
F,6,d1,50
F,6,d2,50
F,6,d3,50
F,7,a1,50
F,7,a2,50
F,7,p1,50
F,7,p2,50
F,7,n1,50
F,7,d1,50
F,7,d2,50
F,7,d3,50
F,8,a1,50
F,8,a2,50
F,8,p1,50
F,8,p2,50
F,8,n1,50
F,8,d1,50
F,8,d2,50
F,8,d3,50
F,9,a1,50
F,9,a2,50
F,9,p1,50
F,9,p2,50
F,9,n1,50
F,9,d1,50
F,9,d2,50
F,9,d3,50
F,10,a1,50
F,10,a2,50
F,10,p1,50
F,10,p2,50
F,10,n1,50
F,10,d1,50
F,10,d2,50
F,10,d3,50
H,3,a1,50
H,3,a2,50
H,3,p1,50
H,3,p2,50
H,3,n1,50
H,3,d1,50
H,3,d2,50
H,3,d3,50
BJ,12,a1,50
BJ,12,a2,50
BJ,12,p1,50
BJ,12,p2,50
BJ,12,n1,50
BJ,12,d1,50
BJ,12,d2,50
BJ,12,d3,50
BJ,13,a1,50
BJ,13,a2,50
BJ,13,p1,50
BJ,13,p2,50
BJ,13,n1,50
BJ,13,d1,50
BJ,13,d2,50
BJ,13,d3,50
BJ,14,a1,50
BJ,14,a2,50
BJ,14,p1,50
BJ,14,p2,50
BJ,14,n1,50
BJ,14,d1,50
BJ,14,d2,50
BJ,14,d3,50
BJ,15,a1,50
BJ,15,a2,50
BJ,15,p1,50
BJ,15,p2,50
BJ,15,n1,50
BJ,15,d1,50
BJ,15,d2,50
BJ,15,d3,50
BJ,19,a1,50
BJ,19,a2,50
BJ,19,p1,50
BJ,19,p2,50
BJ,19,n1,50
BJ,19,d1,50
BJ,19,d2,50
BJ,19,d3,50
BJ,20,a1,50
BJ,20,a2,50
BJ,20,p1,50
BJ,20,p2,50
BJ,20,n1,50
BJ,20,d1,50
BJ,20,d2,50
BJ,20,d3,50
I,12,a1,50
I,12,a2,50
I,12,p1,50
I,12,p2,50
I,12,n1,50
I,12,d1,50
I,12,d2,50
I,12,d3,50
I,13,a1,50
I,13,a2,50
I,13,p1,50
I,13,p2,50
I,13,n1,50
I,13,d1,50
I,13,d2,50
I,13,d3,50
I,14,a1,50
I,14,a2,50
I,14,p1,50
I,14,p2,50
I,14,n1,50
I,14,d1,50
I,14,d2,50
I,14,d3,50
I,15,a1,50
I,15,a2,50
I,15,p1,50
I,15,p2,50
I,15,n1,50
I,15,d1,50
I,15,d2,50
I,15,d3,50
I,16,a1,50
I,16,a2,50
I,16,p1,50
I,16,p2,50
I,16,n1,50
I,16,d1,50
I,16,d2,50
I,16,d3,50
I,17,a1,50
I,17,a2,50
I,17,p1,50
I,17,p2,50
I,17,n1,50
I,17,d1,50
I,17,d2,50
I,17,d3,50
I,18,a1,50
I,18,a2,50
I,18,p1,50
I,18,p2,50
I,18,n1,50
I,18,d1,50
I,18,d2,50
I,18,d3,50
I,19,a1,50
I,19,a2,50
I,19,p1,50
I,19,p2,50
I,19,n1,50
I,19,d1,50
I,19,d2,50
I,19,d3,50
I,20,a1,50
I,20,a2,50
I,20,p1,50
I,20,p2,50
I,20,n1,50
I,20,d1,50
I,20,d2,50
I,20,d3,50
BK,17,a1,50
BK,17,a2,50
BK,17,p1,50
BK,17,p2,50
BK,17,n1,50
BK,17,d1,50
BK,17,d2,50
BK,17,d3,50
K,0,a1,50
K,0,a2,50
K,0,p1,50
K,0,p2,50
K,0,n1,50
K,0,d1,50
K,0,d2,50
K,0,d3,50
K,1,a1,50
K,1,a2,50
K,1,p1,50
K,1,p2,50
K,1,n1,50
K,1,d1,50
K,1,d2,50
K,1,d3,50
BM,2,a1,50
BM,2,a2,50
BM,2,p1,50
BM,2,p2,50
BM,2,n1,50
BM,2,d1,50
BM,2,d2,50
BM,2,d3,50
BM,3,a1,50
BM,3,a2,50
BM,3,p1,50
BM,3,p2,50
BM,3,n1,50
BM,3,d1,50
BM,3,d2,50
BM,3,d3,50
BM,4,a1,50
BM,4,a2,50
BM,4,p1,50
BM,4,p2,50
BM,4,n1,50
BM,4,d1,50
BM,4,d2,50
BM,4,d3,50
BN,3,a1,50
BN,3,a2,50
BN,3,p1,50
BN,3,p2,50
BN,3,n1,50
BN,3,d1,50
BN,3,d2,50
BN,3,d3,50
M,19,a1,50
M,19,a2,50
M,19,p1,50
M,19,p2,50
M,19,n1,50
M,19,d1,50
M,19,d2,50
M,19,d3,50
M,20,a1,50
M,20,a2,50
M,20,p1,50
M,20,p2,50
M,20,n1,50
M,20,d1,50
M,20,d2,50
M,20,d3,50
BO,20,a1,50
BO,20,a2,50
BO,20,p1,50
BO,20,p2,50
BO,20,n1,50
BO,20,d1,50
BO,20,d2,50
BO,20,d3,50
P,4,a1,50
P,4,a2,50
P,4,p1,50
P,4,p2,50
P,4,n1,50
P,4,d1,50
P,4,d2,50
P,4,d3,50
P,5,a1,50
P,5,a2,50
P,5,p1,50
P,5,p2,50
P,5,n1,50
P,5,d1,50
P,5,d2,50
P,5,d3,50
P,6,a1,50
P,6,a2,50
P,6,p1,50
P,6,p2,50
P,6,n1,50
P,6,d1,50
P,6,d2,50
P,6,d3,50
P,7,a1,50
P,7,a2,50
P,7,p1,50
P,7,p2,50
P,7,n1,50
P,7,d1,50
P,7,d2,50
P,7,d3,50
BS,12,a1,50
BS,12,a2,50
BS,12,p1,50
BS,12,p2,50
BS,12,n1,50
BS,12,d1,50
BS,12,d2,50
BS,12,d3,50
BS,13,a1,50
BS,13,a2,50
BS,13,p1,50
BS,13,p2,50
BS,13,n1,50
BS,13,d1,50
BS,13,d2,50
BS,13,d3,50
BS,14,a1,50
BS,14,a2,50
BS,14,p1,50
BS,14,p2,50
BS,14,n1,50
BS,14,d1,50
BS,14,d2,50
BS,14,d3,50
BS,15,a1,50
BS,15,a2,50
BS,15,p1,50
BS,15,p2,50
BS,15,n1,50
BS,15,d1,50
BS,15,d2,50
BS,15,d3,50
BS,16,a1,50
BS,16,a2,50
BS,16,p1,50
BS,16,p2,50
BS,16,n1,50
BS,16,d1,50
BS,16,d2,50
BS,16,d3,50
BS,17,a1,50
BS,17,a2,50
BS,17,p1,50
BS,17,p2,50
BS,17,n1,50
BS,17,d1,50
BS,17,d2,50
BS,17,d3,50
BS,18,a1,50
BS,18,a2,50
BS,18,p1,50
BS,18,p2,50
BS,18,n1,50
BS,18,d1,50
BS,18,d2,50
BS,18,d3,50
BS,19,a1,50
BS,19,a2,50
BS,19,p1,50
BS,19,p2,50
BS,19,n1,50
BS,19,d1,50
BS,19,d2,50
BS,19,d3,50
BT,11,a1,50
BT,11,a2,50
BT,11,p1,50
BT,11,p2,50
BT,11,n1,50
BT,11,d1,50
BT,11,d2,50
BT,11,d3,50
S,15,a1,50
S,15,a2,50
S,15,p1,50
S,15,p2,50
S,15,n1,50
S,15,d1,50
S,15,d2,50
S,15,d3,50
S,16,a1,50
S,16,a2,50
S,16,p1,50
S,16,p2,50
S,16,n1,50
S,16,d1,50
S,16,d2,50
S,16,d3,50
BU,4,a1,50
BU,4,a2,50
BU,4,p1,50
BU,4,p2,50
BU,4,n1,50
BU,4,d1,50
BU,4,d2,50
BU,4,d3,50
T,6,a1,50
T,6,a2,50
T,6,p1,50
T,6,p2,50
T,6,n1,50
T,6,d1,50
T,6,d2,50
T,6,d3,50
T,7,a1,50
T,7,a2,50
T,7,p1,50
T,7,p2,50
T,7,n1,50
T,7,d1,50
T,7,d2,50
T,7,d3,50
T,8,a1,50
T,8,a2,50
T,8,p1,50
T,8,p2,50
T,8,n1,50
T,8,d1,50
T,8,d2,50
T,8,d3,50
T,9,a1,50
T,9,a2,50
T,9,p1,50
T,9,p2,50
T,9,n1,50
T,9,d1,50
T,9,d2,50
T,9,d3,50
T,10,a1,50
T,10,a2,50
T,10,p1,50
T,10,p2,50
T,10,n1,50
T,10,d1,50
T,10,d2,50
T,10,d3,50
T,11,a1,50
T,11,a2,50
T,11,p1,50
T,11,p2,50
T,11,n1,50
T,11,d1,50
T,11,d2,50
T,11,d3,50
T,12,a1,50
T,12,a2,50
T,12,p1,50
T,12,p2,50
T,12,n1,50
T,12,d1,50
T,12,d2,50
T,12,d3,50
T,13,a1,50
T,13,a2,50
T,13,p1,50
T,13,p2,50
T,13,n1,50
T,13,d1,50
T,13,d2,50
T,13,d3,50
BW,11,a1,50
BW,11,a2,50
BW,11,p1,50
BW,11,p2,50
BW,11,n1,50
BW,11,d1,50
BW,11,d2,50
BW,11,d3,50
W,7,a1,50
W,7,a2,50
W,7,p1,50
W,7,p2,50
W,7,n1,50
W,7,d1,50
W,7,d2,50
W,7,d3,50
W,8,a1,50
W,8,a2,50
W,8,p1,50
W,8,p2,50
W,8,n1,50
W,8,d1,50
W,8,d2,50
W,8,d3,50
W,9,a1,50
W,9,a2,50
W,9,p1,50
W,9,p2,50
W,9,n1,50
W,9,d1,50
W,9,d2,50
W,9,d3,50
W,10,a1,50
W,10,a2,50
W,10,p1,50
W,10,p2,50
W,10,n1,50
W,10,d1,50
W,10,d2,50
W,10,d3,50
W,11,a1,50
W,11,a2,50
W,11,p1,50
W,11,p2,50
W,11,n1,50
W,11,d1,50
W,11,d2,50
W,11,d3,50
W,12,a1,50
W,12,a2,50
W,12,p1,50
W,12,p2,50
W,12,n1,50
W,12,d1,50
W,12,d2,50
W,12,d3,50
W,13,a1,50
W,13,a2,50
W,13,p1,50
W,13,p2,50
W,13,n1,50
W,13,d1,50
W,13,d2,50
W,13,d3,50
W,14,a1,50
W,14,a2,50
W,14,p1,50
W,14,p2,50
W,14,n1,50
W,14,d1,50
W,14,d2,50
W,14,d3,50
W,15,a1,50
W,15,a2,50
W,15,p1,50
W,15,p2,50
W,15,n1,50
W,15,d1,50
W,15,d2,50
W,15,d3,50
W,16,a1,50
W,16,a2,50
W,16,p1,50
W,16,p2,50
W,16,n1,50
W,16,d1,50
W,16,d2,50
W,16,d3,50
BZ,11,a1,50
BZ,11,a2,50
BZ,11,p1,50
BZ,11,p2,50
BZ,11,n1,50
BZ,11,d1,50
BZ,11,d2,50
BZ,11,d3,50
CB,4,a1,50
CB,4,a2,50
CB,4,p1,50
CB,4,p2,50
CB,4,n1,50
CB,4,d1,50
CB,4,d2,50
CB,4,d3,50
CB,5,a1,50
CB,5,a2,50
CB,5,p1,50
CB,5,p2,50
CB,5,n1,50
CB,5,d1,50
CB,5,d2,50
CB,5,d3,50
CB,6,a1,50
CB,6,a2,50
CB,6,p1,50
CB,6,p2,50
CB,6,n1,50
CB,6,d1,50
CB,6,d2,50
CB,6,d3,50
CB,10,a1,50
CB,10,a2,50
CB,10,p1,50
CB,10,p2,50
CB,10,n1,50
CB,10,d1,50
CB,10,d2,50
CB,10,d3,50
CB,11,a1,50
CB,11,a2,50
CB,11,p1,50
CB,11,p2,50
CB,11,n1,50
CB,11,d1,50
CB,11,d2,50
CB,11,d3,50
CB,12,a1,50
CB,12,a2,50
CB,12,p1,50
CB,12,p2,50
CB,12,n1,50
CB,12,d1,50
CB,12,d2,50
CB,12,d3,50
CB,13,a1,50
CB,13,a2,50
CB,13,p1,50
CB,13,p2,50
CB,13,n1,50
CB,13,d1,50
CB,13,d2,50
CB,13,d3,50
CB,14,a1,50
CB,14,a2,50
CB,14,p1,50
CB,14,p2,50
CB,14,n1,50
CB,14,d1,50
CB,14,d2,50
CB,14,d3,50
CB,15,a1,50
CB,15,a2,50
CB,15,p1,50
CB,15,p2,50
CB,15,n1,50
CB,15,d1,50
CB,15,d2,50
CB,15,d3,50
CB,16,a1,50
CB,16,a2,50
CB,16,p1,50
CB,16,p2,50
CB,16,n1,50
CB,16,d1,50
CB,16,d2,50
CB,16,d3,50
CB,17,a1,50
CB,17,a2,50
CB,17,p1,50
CB,17,p2,50
CB,17,n1,50
CB,17,d1,50
CB,17,d2,50
CB,17,d3,50
CC,17,a1,50
CC,17,a2,50
CC,17,p1,50
CC,17,p2,50
CC,17,n1,50
CC,17,d1,50
CC,17,d2,50
CC,17,d3,50
CC,18,a1,50
CC,18,a2,50
CC,18,p1,50
CC,18,p2,50
CC,18,n1,50
CC,18,d1,50
CC,18,d2,50
CC,18,d3,50
CC,19,a1,50
CC,19,a2,50
CC,19,p1,50
CC,19,p2,50
CC,19,n1,50
CC,19,d1,50
CC,19,d2,50
CC,19,d3,50
CC,20,a1,50
CC,20,a2,50
CC,20,p1,50
CC,20,p2,50
CC,20,n1,50
CC,20,d1,50
CC,20,d2,50
CC,20,d3,50
CF,10,a1,50
CF,10,a2,50
CF,10,p1,50
CF,10,p2,50
CF,10,n1,50
CF,10,d1,50
CF,10,d2,50
CF,10,d3,50
CF,11,a1,50
CF,11,a2,50
CF,11,p1,50
CF,11,p2,50
CF,11,n1,50
CF,11,d1,50
CF,11,d2,50
CF,11,d3,50
CF,12,a1,50
CF,12,a2,50
CF,12,p1,50
CF,12,p2,50
CF,12,n1,50
CF,12,d1,50
CF,12,d2,50
CF,12,d3,50
CF,13,a1,50
CF,13,a2,50
CF,13,p1,50
CF,13,p2,50
CF,13,n1,50
CF,13,d1,50
CF,13,d2,50
CF,13,d3,50
CF,14,a1,50
CF,14,a2,50
CF,14,p1,50
CF,14,p2,50
CF,14,n1,50
CF,14,d1,50
CF,14,d2,50
CF,14,d3,50
CF,15,a1,50
CF,15,a2,50
CF,15,p1,50
CF,15,p2,50
CF,15,n1,50
CF,15,d1,50
CF,15,d2,50
CF,15,d3,50
CF,16,a1,50
CF,16,a2,50
CF,16,p1,50
CF,16,p2,50
CF,16,n1,50
CF,16,d1,50
CF,16,d2,50
CF,16,d3,50
CF,17,a1,50
CF,17,a2,50
CF,17,p1,50
CF,17,p2,50
CF,17,n1,50
CF,17,d1,50
CF,17,d2,50
CF,17,d3,50
CF,18,a1,50
CF,18,a2,50
CF,18,p1,50
CF,18,p2,50
CF,18,n1,50
CF,18,d1,50
CF,18,d2,50
CF,18,d3,50
CF,19,a1,50
CF,19,a2,50
CF,19,p1,50
CF,19,p2,50
CF,19,n1,50
CF,19,d1,50
CF,19,d2,50
CF,19,d3,50
CF,20,a1,50
CF,20,a2,50
CF,20,p1,50
CF,20,p2,50
CF,20,n1,50
CF,20,d1,50
CF,20,d2,50
CF,20,d3,50
CH,2,a1,50
CH,2,a2,50
CH,2,p1,50
CH,2,p2,50
CH,2,n1,50
CH,2,d1,50
CH,2,d2,50
CH,2,d3,50
CI,12,a1,50
CI,12,a2,50
CI,12,p1,50
CI,12,p2,50
CI,12,n1,50
CI,12,d1,50
CI,12,d2,50
CI,12,d3,50
CI,13,a1,50
CI,13,a2,50
CI,13,p1,50
CI,13,p2,50
CI,13,n1,50
CI,13,d1,50
CI,13,d2,50
CI,13,d3,50
CI,14,a1,50
CI,14,a2,50
CI,14,p1,50
CI,14,p2,50
CI,14,n1,50
CI,14,d1,50
CI,14,d2,50
CI,14,d3,50
CI,15,a1,50
CI,15,a2,50
CI,15,p1,50
CI,15,p2,50
CI,15,n1,50
CI,15,d1,50
CI,15,d2,50
CI,15,d3,50
CI,16,a1,50
CI,16,a2,50
CI,16,p1,50
CI,16,p2,50
CI,16,n1,50
CI,16,d1,50
CI,16,d2,50
CI,16,d3,50
CI,17,a1,50
CI,17,a2,50
CI,17,p1,50
CI,17,p2,50
CI,17,n1,50
CI,17,d1,50
CI,17,d2,50
CI,17,d3,50
CK,20,a1,50
CK,20,a2,50
CK,20,p1,50
CK,20,p2,50
CK,20,n1,50
CK,20,d1,50
CK,20,d2,50
CK,20,d3,50
CQ,20,a1,50
CQ,20,a2,50
CQ,20,p1,50
CQ,20,p2,50
CQ,20,n1,50
CQ,20,d1,50
CQ,20,d2,50
CQ,20,d3,50
CR,9,a1,50
CR,9,a2,50
CR,9,p1,50
CR,9,p2,50
CR,9,n1,50
CR,9,d1,50
CR,9,d2,50
CR,9,d3,50
CR,10,a1,50
CR,10,a2,50
CR,10,p1,50
CR,10,p2,50
CR,10,n1,50
CR,10,d1,50
CR,10,d2,50
CR,10,d3,50
CR,11,a1,50
CR,11,a2,50
CR,11,p1,50
CR,11,p2,50
CR,11,n1,50
CR,11,d1,50
CR,11,d2,50
CR,11,d3,50
CR,12,a1,50
CR,12,a2,50
CR,12,p1,50
CR,12,p2,50
CR,12,n1,50
CR,12,d1,50
CR,12,d2,50
CR,12,d3,50
CR,13,a1,50
CR,13,a2,50
CR,13,p1,50
CR,13,p2,50
CR,13,n1,50
CR,13,d1,50
CR,13,d2,50
CR,13,d3,50
CR,14,a1,50
CR,14,a2,50
CR,14,p1,50
CR,14,p2,50
CR,14,n1,50
CR,14,d1,50
CR,14,d2,50
CR,14,d3,50
CR,15,a1,50
CR,15,a2,50
CR,15,p1,50
CR,15,p2,50
CR,15,n1,50
CR,15,d1,50
CR,15,d2,50
CR,15,d3,50
CR,16,a1,50
CR,16,a2,50
CR,16,p1,50
CR,16,p2,50
CR,16,n1,50
CR,16,d1,50
CR,16,d2,50
CR,16,d3,50
CR,17,a1,50
CR,17,a2,50
CR,17,p1,50
CR,17,p2,50
CR,17,n1,50
CR,17,d1,50
CR,17,d2,50
CR,17,d3,50
CR,18,a1,50
CR,18,a2,50
CR,18,p1,50
CR,18,p2,50
CR,18,n1,50
CR,18,d1,50
CR,18,d2,50
CR,18,d3,50
CR,19,a1,50
CR,19,a2,50
CR,19,p1,50
CR,19,p2,50
CR,19,n1,50
CR,19,d1,50
CR,19,d2,50
CR,19,d3,50
CR,20,a1,50
CR,20,a2,50
CR,20,p1,50
CR,20,p2,50
CR,20,n1,50
CR,20,d1,50
CR,20,d2,50
CR,20,d3,50
CS,5,a1,50
CS,5,a2,50
CS,5,p1,50
CS,5,p2,50
CS,5,n1,50
CS,5,d1,50
CS,5,d2,50
CS,5,d3,50
CS,6,a1,50
CS,6,a2,50
CS,6,p1,50
CS,6,p2,50
CS,6,n1,50
CS,6,d1,50
CS,6,d2,50
CS,6,d3,50
CS,7,a1,50
CS,7,a2,50
CS,7,p1,50
CS,7,p2,50
CS,7,n1,50
CS,7,d1,50
CS,7,d2,50
CS,7,d3,50
CS,8,a1,50
CS,8,a2,50
CS,8,p1,50
CS,8,p2,50
CS,8,n1,50
CS,8,d1,50
CS,8,d2,50
CS,8,d3,50
CS,9,a1,50
CS,9,a2,50
CS,9,p1,50
CS,9,p2,50
CS,9,n1,50
CS,9,d1,50
CS,9,d2,50
CS,9,d3,50
CS,10,a1,50
CS,10,a2,50
CS,10,p1,50
CS,10,p2,50
CS,10,n1,50
CS,10,d1,50
CS,10,d2,50
CS,10,d3,50
CS,11,a1,50
CS,11,a2,50
CS,11,p1,50
CS,11,p2,50
CS,11,n1,50
CS,11,d1,50
CS,11,d2,50
CS,11,d3,50
CS,12,a1,50
CS,12,a2,50
CS,12,p1,50
CS,12,p2,50
CS,12,n1,50
CS,12,d1,50
CS,12,d2,50
CS,12,d3,50
CS,13,a1,50
CS,13,a2,50
CS,13,p1,50
CS,13,p2,50
CS,13,n1,50
CS,13,d1,50
CS,13,d2,50
CS,13,d3,50
CS,14,a1,50
CS,14,a2,50
CS,14,p1,50
CS,14,p2,50
CS,14,n1,50
CS,14,d1,50
CS,14,d2,50
CS,14,d3,50
CS,15,a1,50
CS,15,a2,50
CS,15,p1,50
CS,15,p2,50
CS,15,n1,50
CS,15,d1,50
CS,15,d2,50
CS,15,d3,50
CV,11,a1,50
CV,11,a2,50
CV,11,p1,50
CV,11,p2,50
CV,11,n1,50
CV,11,d1,50
CV,11,d2,50
CV,11,d3,50
CV,12,a1,50
CV,12,a2,50
CV,12,p1,50
CV,12,p2,50
CV,12,n1,50
CV,12,d1,50
CV,12,d2,50
CV,12,d3,50
CV,19,a1,50
CV,19,a2,50
CV,19,p1,50
CV,19,p2,50
CV,19,n1,50
CV,19,d1,50
CV,19,d2,50
CV,19,d3,50
CV,20,a1,50
CV,20,a2,50
CV,20,p1,50
CV,20,p2,50
CV,20,n1,50
CV,20,d1,50
CV,20,d2,50
CV,20,d3,50

SECTION_COVER
# Day, ShiftID, Requirement, Weight for under, Weight for over
0,a1,8,100,1
0,a2,3,100,1
0,d1,7,100,1
0,d2,6,100,1
0,d3,7,100,1
0,p1,6,100,1
0,p2,4,100,1
0,n1,6,100,1
1,a1,7,100,1
1,a2,5,100,1
1,d1,6,100,1
1,d2,8,100,1
1,d3,6,100,1
1,p1,6,100,1
1,p2,4,100,1
1,n1,5,100,1
2,a1,6,100,1
2,a2,4,100,1
2,d1,6,100,1
2,d2,7,100,1
2,d3,6,100,1
2,p1,7,100,1
2,p2,3,100,1
2,n1,9,100,1
3,a1,6,100,1
3,a2,4,100,1
3,d1,8,100,1
3,d2,7,100,1
3,d3,9,100,1
3,p1,8,100,1
3,p2,4,100,1
3,n1,7,100,1
4,a1,8,100,1
4,a2,4,100,1
4,d1,6,100,1
4,d2,6,100,1
4,d3,5,100,1
4,p1,7,100,1
4,p2,4,100,1
4,n1,8,100,1
5,a1,8,100,1
5,a2,3,100,1
5,d1,5,100,1
5,d2,6,100,1
5,d3,6,100,1
5,p1,5,100,1
5,p2,5,100,1
5,n1,6,100,1
6,a1,6,100,1
6,a2,4,100,1
6,d1,6,100,1
6,d2,6,100,1
6,d3,6,100,1
6,p1,6,100,1
6,p2,4,100,1
6,n1,6,100,1
7,a1,8,100,1
7,a2,4,100,1
7,d1,7,100,1
7,d2,7,100,1
7,d3,8,100,1
7,p1,7,100,1
7,p2,5,100,1
7,n1,7,100,1
8,a1,6,100,1
8,a2,4,100,1
8,d1,6,100,1
8,d2,5,100,1
8,d3,6,100,1
8,p1,8,100,1
8,p2,4,100,1
8,n1,7,100,1
9,a1,7,100,1
9,a2,6,100,1
9,d1,7,100,1
9,d2,7,100,1
9,d3,7,100,1
9,p1,6,100,1
9,p2,4,100,1
9,n1,6,100,1
10,a1,6,100,1
10,a2,4,100,1
10,d1,4,100,1
10,d2,7,100,1
10,d3,6,100,1
10,p1,5,100,1
10,p2,4,100,1
10,n1,7,100,1
11,a1,7,100,1
11,a2,4,100,1
11,d1,7,100,1
11,d2,7,100,1
11,d3,7,100,1
11,p1,6,100,1
11,p2,4,100,1
11,n1,8,100,1
12,a1,7,100,1
12,a2,3,100,1
12,d1,6,100,1
12,d2,7,100,1
12,d3,7,100,1
12,p1,7,100,1
12,p2,6,100,1
12,n1,7,100,1
13,a1,7,100,1
13,a2,4,100,1
13,d1,8,100,1
13,d2,6,100,1
13,d3,6,100,1
13,p1,5,100,1
13,p2,5,100,1
13,n1,6,100,1
14,a1,6,100,1
14,a2,5,100,1
14,d1,6,100,1
14,d2,6,100,1
14,d3,6,100,1
14,p1,7,100,1
14,p2,4,100,1
14,n1,5,100,1
15,a1,7,100,1
15,a2,4,100,1
15,d1,7,100,1
15,d2,8,100,1
15,d3,8,100,1
15,p1,7,100,1
15,p2,4,100,1
15,n1,5,100,1
16,a1,8,100,1
16,a2,2,100,1
16,d1,7,100,1
16,d2,8,100,1
16,d3,6,100,1
16,p1,8,100,1
16,p2,4,100,1
16,n1,5,100,1
17,a1,6,100,1
17,a2,3,100,1
17,d1,4,100,1
17,d2,5,100,1
17,d3,6,100,1
17,p1,6,100,1
17,p2,4,100,1
17,n1,7,100,1
18,a1,8,100,1
18,a2,4,100,1
18,d1,7,100,1
18,d2,6,100,1
18,d3,9,100,1
18,p1,5,100,1
18,p2,4,100,1
18,n1,7,100,1
19,a1,7,100,1
19,a2,6,100,1
19,d1,8,100,1
19,d2,7,100,1
19,d3,7,100,1
19,p1,7,100,1
19,p2,6,100,1
19,n1,6,100,1
20,a1,6,100,1
20,a2,4,100,1
20,d1,6,100,1
20,d2,6,100,1
20,d3,6,100,1
20,p1,6,100,1
20,p2,4,100,1
20,n1,6,100,1

