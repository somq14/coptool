# This is a comment. Comments start with #
SECTION_HORIZON
# All instances start on a Monday
# The horizon length in days:
14

SECTION_SHIFTS
# ShiftID, Length in mins, Shifts which cannot follow this shift | separated
L,480,E
E,480,

SECTION_STAFF
# ID, MaxShifts, MaxTotalMinutes, MinTotalMinutes, MaxConsecutiveShifts, MinConsecutiveShifts, MinConsecutiveDaysOff, MaxWeekends
A,L=14|E=14,5400,2835,5,2,2,1
B,L=0|E=14,5400,2835,5,2,2,1
C,L=14|E=14,5400,2835,5,2,2,1
D,L=14|E=14,5400,2835,5,2,2,1
E,L=14|E=14,5400,2835,5,2,2,1
F,L=14|E=0,5400,2835,5,2,2,1
G,L=14|E=14,5400,2835,5,2,2,1
H,L=14|E=14,5400,2835,5,2,2,1
I,L=14|E=14,5400,2835,5,2,2,1
J,L=14|E=14,5400,2835,5,2,2,1

SECTION_DAYS_OFF
# EmployeeID, DayIndexes (start at zero)
A
B
C
D
E
F
G
H
I
J

SECTION_SHIFT_ON_REQUESTS
# EmployeeID, Day, ShiftID, Weight
A,7,L,2
A,8,L,2
B,7,E,2
B,8,E,2
B,9,E,2
C,1,E,3
C,2,E,3
C,6,E,3
C,7,E,3
C,8,E,3
D,7,E,2
D,8,E,2
D,9,E,2
D,10,E,2
D,11,E,2
E,2,E,3
E,3,E,3
E,4,E,3
E,5,E,3
F,13,L,3
G,6,L,3
G,7,L,3
G,8,L,3
G,9,L,3
G,10,L,3
H,13,E,3
I,0,E,2
I,1,E,2
I,2,E,2
I,10,L,2
I,11,L,2
I,12,L,2
I,13,L,2

SECTION_SHIFT_OFF_REQUESTS
# EmployeeID, Day, ShiftID, Weight
F,3,L,1
F,4,L,1
F,5,L,1
F,6,L,1
J,7,L,3
J,8,L,3
J,9,L,3
J,13,L,1
A,5,L,50
A,5,E,50
A,6,L,50
A,6,E,50
B,11,L,50
B,11,E,50
C,13,L,50
C,13,E,50
D,2,L,50
D,2,E,50
D,12,L,50
D,12,E,50
F,8,L,50
F,8,E,50
H,10,L,50
H,10,E,50
H,11,L,50
H,11,E,50

SECTION_COVER
# Day, ShiftID, Requirement, Weight for under, Weight for over
0,E,2,100,1
0,L,3,100,1
1,E,3,100,1
1,L,3,100,1
2,E,4,100,1
2,L,4,100,1
3,E,2,100,1
3,L,3,100,1
4,E,2,100,1
4,L,3,100,1
5,E,2,100,1
5,L,3,100,1
6,E,2,100,1
6,L,3,100,1
7,E,3,100,1
7,L,3,100,1
8,E,4,100,1
8,L,3,100,1
9,E,3,100,1
9,L,2,100,1
10,E,2,100,1
10,L,3,100,1
11,E,4,100,1
11,L,3,100,1
12,E,2,100,1
12,L,3,100,1
13,E,5,100,1
13,L,3,100,1

